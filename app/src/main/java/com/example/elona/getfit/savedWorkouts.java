package com.example.elona.getfit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class savedWorkouts extends BaseMenuClass implements View.OnClickListener
{
    TextView tvUserMsg, tvNoWorkoutsMsg;
    LinearLayout llDispSavedWorkouts;
    LinearLayout.LayoutParams buttonParams;

    Workout[] allSavedWorkouts;
    WorkoutStorage work;
    int numWorkouts = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_workouts);

        //find GUI elements using XML id
        tvUserMsg = (TextView) findViewById(R.id.tvUserMsg);
        llDispSavedWorkouts = (LinearLayout) findViewById(R.id.llDispSavedWorkouts);

        //create new parameters to apply to programmatically created buttons
        buttonParams = new LinearLayout.LayoutParams(960, LinearLayout.LayoutParams.WRAP_CONTENT);

        //get all saved workouts from database
        work = new WorkoutStorage(getApplicationContext());
        allSavedWorkouts = work.getAllSaved(1);

        //set numworkouts length to number of saved workouts
        numWorkouts = allSavedWorkouts.length;
        //if there are no saved workouts, print message
        if (numWorkouts == 0)
        {
            TextView tvNoWorkouts = new TextView(this);
            tvNoWorkouts.setText("You have not completed any workouts");
            tvNoWorkouts.setLayoutParams(buttonParams);
            llDispSavedWorkouts.addView(tvNoWorkouts);
        }

        //for each saved workout,
        //set button id, and add button to view
        //get workout data from saved workouts array
        //create button with text "workout name  date completed"
        int j = 0;
        for(int i = 0; i < numWorkouts; i++)
        {
            Button bPastWorkout = new Button(this);
            bPastWorkout.setId(j + 1);
            bPastWorkout.setLayoutParams(buttonParams);
            llDispSavedWorkouts.addView(bPastWorkout);
            bPastWorkout.setOnClickListener(this);

            Workout currentWorkout;
            currentWorkout = allSavedWorkouts[i];
            String workoutName = currentWorkout.GetWorkout_Name();
            String workoutDate = currentWorkout.GetDate();
            bPastWorkout.setText(workoutName + " " + workoutDate);
        }
    }

    @Override
    public void onClick(View v)
    {
        //get id of the button that was selected
        int buttonID = v.getId();
        Workout workout = work.getWorkout(allSavedWorkouts[buttonID-1].workoutname);

        //pass workout and current activity to next activity
        Intent i = new Intent(this, workoutOverview.class);
        i.putExtra("workout",workout);
        i.putExtra("prevActivity", 1);
        startActivity(i);
    }
}
