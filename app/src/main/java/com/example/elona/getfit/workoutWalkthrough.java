package com.example.elona.getfit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import android.os.Handler;
import android.os.SystemClock;
import android.os.CountDownTimer;


public class workoutWalkthrough extends BaseMenuClass implements View.OnClickListener
{
    //initialize exercisenames/exercisearray arrays, duration, difficulty, and i (counter) ints
    String[] musclegroups;
    Exercise [] exercisearray;
    int duration;
    int difficulty;
    int i = 0;
    String workoutName;
    String timestring;
    int preActivity;
    int metscore;
    ArrayList<Exercise> exerciseList;

    int time;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    //initialize GUI elements progress bar, button and textviews
    ProgressBar progressBar;
    Button bWalkthrough;
    TextView tvWorkoutName, tvExerciseName, tvExerciseReps, tvExerciseSets;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_walkthrough);

        //get extras from last activity
        Intent intent = getIntent();
        difficulty = intent.getIntExtra("int_difficulty", 1);
        workoutName = intent.getStringExtra("string-workoutname");
        preActivity = intent.getIntExtra("prevActivity", 0);
        musclegroups = intent.getStringArrayExtra("musclegroups");
        metscore = intent.getIntExtra("metscore",0);

        //get exercises from previous activity
        exerciseList = (ArrayList<Exercise>)getIntent().getSerializableExtra("exerciselist");
        exercisearray = new Exercise[exerciseList.size()];
        exerciseList.toArray(exercisearray);

        //start timer
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);

        //duration is the number of elements in exercisenames array
        duration = exerciseList.size();

        //find GUI elements by id from the XML layout file
        bWalkthrough = (Button) findViewById(R.id.bWalkthrough);
        tvWorkoutName = (TextView) findViewById(R.id.tvWorkoutName);
        tvExerciseName = (TextView) findViewById(R.id.tvExerciseName);
        tvExerciseReps = (TextView) findViewById(R.id.tvExerciseReps);
        tvExerciseSets = (TextView) findViewById(R.id.tvExerciseSets);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(duration);
        progressBar.setScaleY(3f);

        tvWorkoutName.setText(workoutName);

        //set listener for walkthrough button
        bWalkthrough.setOnClickListener(this);

        //instance of first exercise object
        progressBar.setProgress(i);

        //if exercise is jogging it displays a count down timer
        if(exercisearray[i].GetExercisename().equals("Jogging"))
        {
            tvExerciseName.setText(exercisearray[i].GetExercisename());
            tvExerciseSets.setVisibility(View.GONE);
            bWalkthrough.setVisibility(View.GONE);

            new CountDownTimer((exercisearray[i].time * 60000), 1000) {

                //counting down
                public void onTick(long millisUntilFinished)
                {
                    tvExerciseReps.setText("" + "Time Remaining: " + (millisUntilFinished / (1000 * 60)) % 60 + " mins");
                }
                public void onFinish()
                {
                    tvExerciseReps.setText("" + "Time Remaining: 0");
                    bWalkthrough.setVisibility(View.VISIBLE);
                    if (i+1 == duration)
                    {
                        bWalkthrough.setText("Done");
                    }
                }
            }.start();
        }
        else
        {
            tvExerciseName.setText(exercisearray[i].GetExercisename());
            tvExerciseReps.setText("" + exercisearray[i].GetReps() + " reps");
            tvExerciseSets.setText("" + exercisearray[i].GetSets() + " sets");
        }
    }

    @Override
    public void onClick(View v)
    {
        //get id of the button that was selected
        switch (v.getId())
        {
            //if the button selected is the walkthrough button
            case R.id.bWalkthrough:

                //if the end of the workout has not been reached yet,
                if (i+1 < duration)
                {
                    //loop through objects of exercise and displaying info
                    i = i + 1;

                    //if exercise is jogging it displays a count down timer
                    if(exercisearray[i].GetExercisename().equals("Jogging"))
                    {
                        tvExerciseName.setText(exercisearray[i].GetExercisename());
                        tvExerciseSets.setVisibility(View.GONE);
                        bWalkthrough.setVisibility(View.GONE);
                        new CountDownTimer((exercisearray[i].time * 60000), 1000) {

                            //counting down
                            public void onTick(long millisUntilFinished) {
                                tvExerciseReps.setText("Time Remaining: " + (millisUntilFinished / (1000 * 60)) % 60 + " mins");
                            }
                            public void onFinish()
                            {
                                tvExerciseReps.setText("" + "Time Remaining: 0");
                                bWalkthrough.setVisibility(View.VISIBLE);
                                if (i+1 == duration)
                                {
                                    bWalkthrough.setText("Done");
                                }
                            }
                        }.start();

                    }
                    else
                    {
                        tvExerciseName.setText(exercisearray[i].GetExercisename());
                        tvExerciseReps.setText("" + exercisearray[i].GetReps() + " reps");
                        tvExerciseSets.setText("" + exercisearray[i].GetSets() + " sets");
                        progressBar.setProgress(i);

                        //if the user is at the last exercise of the workout,
                        //change the text of the walkthrough button from "continue" to "done"
                        if (i+1 == duration)
                        {
                            bWalkthrough.setText("Done");
                        }
                    }
                }

                //if the text on the walkthrough button has already been changed to "done",
                //the user has completed the last exercise, and the button will now call the workout stats activity
                else if (bWalkthrough.getText()=="Done")
                {
                    //stopping timer
                    timeSwapBuff += timeInMilliseconds;
                    customHandler.removeCallbacks(updateTimerThread);

                    //passing names and difficulty to new activity
                    Bundle b=new Bundle();
                    ArrayList<Exercise> exerciseList = new ArrayList<>(Arrays.asList(exercisearray));
                    Intent i = new Intent(this, workoutStats.class);
                    i.putExtra("int_difficulty",difficulty);
                    i.putExtra("string_workoutname",workoutName);
                    i.putExtra("musclegroups",musclegroups);
                    i.putExtra("prevActivity",preActivity);
                    i.putExtra("exerciselist", exerciseList);
                    i.putExtra("time",time);
                    i.putExtra("metscore",metscore);
                    i.putExtra("stringtime",timestring);
                    i.putExtras(b);
                    startActivity(i);
                }
                break;
        }
    }

    //workout timer
    private Runnable updateTimerThread = new Runnable() {
        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            time = (mins + (secs / 60));
            timestring = ("" + mins + ":" + String.format("%02d", secs) + ":" + String.format("%03d", milliseconds));

            customHandler.postDelayed(this, 0);
        }

    };
}
