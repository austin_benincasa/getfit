package com.example.elona.getfit;
/**
 * Created by Austin on 10/26/15.
 */
public class User
{
    //user variables
    int age, height, weight, gender;
    String firstname, lastname, username, password;

    //empty constructor
    public User() {
    }

    //user constructor
    public User(String username,String password,String firstname, String lastname,int age, int height,
                int weight, int gender)
    {
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.gender = gender;
    }
    //set age
    public void SetAge(int age)
    {
        this.age = age;
    }
    //get age
    public int GetAge()
    {
        return this.age;
    }
    //set height
    public void SetHeight(int height)
    {
        this.height = height;
    }
    //get height
    public int GetHeight()
    {
        return this.height;
    }
    //set weight
    public void SetWeight(int weight)
    {
        this.weight = weight;
    }
    //get weight
    public int GetWeight()
    {
        return this.weight;
    }
    //set first name
    public void SetFirstname(String firstname)
    {
        this.firstname = firstname;
    }
    //get first name
    public String GetFirstname()
    {
        return this.firstname;
    }
    //set last name
    public void SetLastname(String lastname)
    {
        this.lastname = lastname;
    }
    //get last name
    public String GetLastname()
    {
        return this.lastname;
    }
    //set username
    public void SetUsername(String username)
    {
        this.username = username;
    }
    //get username
    public String GetUsername()
    {
        return this.username;
    }
    //set password
    public void SetPassword(String password)
    {
        this.password = password;
    }
    //get password
    public String GetPassword() {
        return this.password;
    }
    //set gender
    public void SetGender(int gender)
    {
        this.gender = gender;
    }
    //get gender
    public int GetGender()
    {
        return this.gender;
    }
}
