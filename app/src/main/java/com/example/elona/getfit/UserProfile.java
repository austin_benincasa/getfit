package com.example.elona.getfit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//public class UserProfile extends AppCompatActivity implements View.OnClickListener
public class UserProfile extends BaseMenuClass implements View.OnClickListener
{

    Button bEdit, bSaveChanges;
    EditText etFirstName, etLastName, etGender, etAge, etHeight, etWeight;
    TextView tvUserName;
    User user;
    public UserStorage us;
    public String username;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        //get user name from sharedpreferences
        SharedPreferences prefs = getSharedPreferences("USER_PREFS", MODE_PRIVATE);
        String username = prefs.getString("username", null);

        us = new UserStorage(this);

        //find GUI elements using ids from .xml files
        tvUserName = (TextView) findViewById(R.id.tvUserName);

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etGender = (EditText) findViewById(R.id.etGender);
        etAge = (EditText) findViewById(R.id.etAge);
        etHeight = (EditText) findViewById(R.id.etHeight);
        etWeight = (EditText) findViewById(R.id.etWeight);

        bEdit = (Button) findViewById(R.id.bEdit);
        bSaveChanges = (Button) findViewById(R.id.bSaveChanges);
        //set save changes button to invisible initially
        bSaveChanges.setVisibility(View.GONE);

        //set all edit texts to uneditable initially
        //to edit, user will hit edit button
        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etGender.setEnabled(false);
        etAge.setEnabled(false);
        etHeight.setEnabled(false);
        etWeight.setEnabled(false);

        //set listener for buttons
        bSaveChanges.setOnClickListener(this);
        bEdit.setOnClickListener(this);

        //get user from database by using username
        user = us.getUser(username);

        //set gender after retrieving from user object
        //convert from int to string
        String gender = String.valueOf(user.GetGender());
        if (gender.equals("1"))
        {
            gender = "F";
        }
        else
        {
            gender = "M";
        }

        //set textviews to display user information
        tvUserName.setText(user.GetUsername());
        etFirstName.setText(user.GetFirstname());
        etLastName.setText(user.GetLastname());
        etGender.setText(gender);
        etAge.setText(String.valueOf(user.GetAge()));
        etHeight.setText(String.valueOf(user.GetHeight()));
        etWeight.setText(String.valueOf(user.GetWeight()));
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId()){

            //if the user clicks the edit button
            case R.id.bEdit:

                //set edit texts to editable
                etFirstName.setEnabled(true);
                etLastName.setEnabled(true);
                etGender.setEnabled(true);
                etAge.setEnabled(true);
                etHeight.setEnabled(true);
                etWeight.setEnabled(true);

                //get rid of edit button, set save changes button to visible
                bEdit.setVisibility(View.GONE);
                bSaveChanges.setVisibility(View.VISIBLE);
                break;

            //if button clicked is save changes
            case R.id.bSaveChanges:

                //get updated text
                String fname = etFirstName.getText().toString();
                String lname = etLastName.getText().toString();

                //convert gender from string to int
                int gender;
                if(etGender.getText().toString().equals("M"))
                {
                    gender = 0;
                }
                else
                {
                    gender = 1;
                }

                //get age, height and weight from udated edit texts
                int age = Integer.parseInt(etAge.getText().toString());
                int height = Integer.parseInt(etHeight.getText().toString());
                int weight = Integer.parseInt(etWeight.getText().toString());

                //create new user, delete old user and add new user with updated user
                User newuser = new User(user.GetUsername(),user.GetPassword(),fname,lname,age,height,weight,gender);
                us.deleteEntry(username);
                us.addUser(newuser);

                //take user to main screen
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                this.finish();
                break;
        }
    }
}
