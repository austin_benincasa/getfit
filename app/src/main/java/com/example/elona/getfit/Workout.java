package com.example.elona.getfit;

/**
 * Created by Austin on 10/26/15.
 */
import java.io.Serializable;
import java.util.*;

public class Workout implements Serializable {

    String workoutname = "";
    String smusclegroups = "";
    String date = "";
    ArrayList <String> Exercises;
    int duration;
    String exercisenames = "";
    String setsreps = "";
    ArrayList <String> SetsReps;
    ArrayList <String> Musclegroups;
    int difficulty;
    int saved;

    //empty constructor
    public Workout() {
    }

    //workout constructor
    public Workout(String workoutname, ArrayList <String> Exercises, ArrayList <String> SetsReps,ArrayList <String> Musclegroups, String date, int duration,int saved)
    {

        this.workoutname = workoutname;
        this.date = date;
        this.Exercises = Exercises;
        this.Musclegroups = Musclegroups;
        this.SetsReps = SetsReps;
        this.duration = duration;
        this.saved = saved;
    }

    //workout constructor
    public Workout(String workoutname, ArrayList <String> Exercises, String date, int difficulty)
    {
        this.workoutname = workoutname;
        this.date = date;
        this.Exercises = Exercises;
        this.difficulty = difficulty;
    }

    //set and get difficulty
    public void SetDifficulty(int difficulty)
    {
        this.difficulty = difficulty;
    }
    public int GetDifficulty()
    {
        return this.difficulty;
    }

    //set and get duration
    public void SetDuration(int duration)
    {
        this.duration = duration;
    }
    public int GetDuration()
    {
        return this.duration;
    }

    //set and get saved status
    public int GetSaved()
    {
        return this.saved;
    }
    public void SetSaved(int saved)
    {
        this.saved = saved;
    }

    //set and get workout names
    public void SetWorkout_Name(String workoutname)
    {
        this.workoutname = workoutname;
    }
    public String GetWorkout_Name()
    {
        return this.workoutname;
    }

    //set and get workout date
    public void SetDate(String date)
    {
        this.date = date;
    }
    public String GetDate()
    {
        return this.date;
    }

    //set and get sets and reps
    public void SetSetsReps(ArrayList<String> SetsReps)
    {
        this.SetsReps = SetsReps ;
    }
    public String GetSetsReps()
    {
        if(SetsReps != null)
        {
            for (int i = 0; i < SetsReps.size(); i++)
            {
                setsreps += SetsReps.get(i);
                if((i + 1) < SetsReps.size())
                {
                    setsreps += ",";
                }
            }
            return setsreps;
        }
        else
        {
            return setsreps;
        }
    }

    public void SetMuscleGroups(ArrayList<String> Musclegroups)
    {
        this.Musclegroups = Musclegroups ;
    }

    public String GetMuscleGroups()
    {
        if(Musclegroups != null)
        {
            for (int i = 0; i < Musclegroups.size(); i++)
            {
                smusclegroups += Musclegroups.get(i);
                if((i + 1) < Musclegroups.size())
                {
                    smusclegroups += ",";
                }
            }
            return smusclegroups;
        }
        else
        {
            return smusclegroups;
        }
    }

    public void SetExercise(ArrayList<String> Exercises)
    {
        this.Exercises = Exercises;
    }

    public String GetExercise()
    {
        if(Exercises != null) {
            for (int i = 0; i < Exercises.size(); i++) {
                exercisenames += Exercises.get(i);
                if ((i + 1) < Exercises.size()) {
                    exercisenames += ",";
                }
            }
            return exercisenames;
        }
        else
        {
            return exercisenames;
        }

    }
}
