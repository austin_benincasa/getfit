package com.example.elona.getfit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.DatabaseUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Austin on 10/27/15.
 */
public class WorkoutStorage extends SQLiteOpenHelper {

        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "Workout Table";

        //Workout table values
        private static final String TABLE_WORKOUT = "Workouts";
        private static final String KEY_ID = "id";
        private static final String KEY_WORKOUT_NAME = "name";
        private static final String KEY_DATE = "date";
        private static final String KEY_SETS_REPS = "sets_reps";
        private static final String KEY_EXERCISES = "exercises";
        private static final String KEY_DURATION = "duration";
        private static final String KEY_SAVED = "saved";
        private static final String KEY_MUSCLE_GROUP = "muscle_groups";

        public WorkoutStorage(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            String CREATE_WORKOUT_TABLE = "CREATE TABLE " + TABLE_WORKOUT + "(" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_WORKOUT_NAME + " TEXT," + KEY_DATE + " TEXT,"
                    + KEY_SETS_REPS + " TEXT," + KEY_EXERCISES + " TEXT,"+ KEY_MUSCLE_GROUP + " TEXT," + KEY_DURATION + " INTEGER," + KEY_SAVED + " INTEGER" + ");";
            db.execSQL(CREATE_WORKOUT_TABLE);
        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORKOUT);
            onCreate(db);
        }

        //adding workout to database
        public long addWorkout(Workout workout)
        {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(KEY_WORKOUT_NAME, workout.GetWorkout_Name());
            values.put(KEY_DATE, workout.GetDate());
            values.put(KEY_SETS_REPS, workout.GetSetsReps());
            values.put(KEY_EXERCISES, workout.GetExercise());
            values.put(KEY_DURATION, workout.GetDuration());
            values.put(KEY_SAVED, workout.GetSaved());
            values.put(KEY_MUSCLE_GROUP, workout.GetMuscleGroups());

            long insert = db.insert(TABLE_WORKOUT, null, values);
            return insert;

        }
        //getting number of workouts in database
        public int getnumberofworkouts()
        {
            long numRows;
            SQLiteDatabase db = this.getReadableDatabase();
            try {
                numRows = DatabaseUtils.queryNumEntries(db, TABLE_WORKOUT);
            }
            catch (SQLiteException e)
            {
                throw new Error("Unable to get data from database");
            }

            int rows = (int) numRows;
            return rows;
        }
        public ArrayList<Integer> getArraylistSetsReps(String workoutname)
        {
            ArrayList<Integer> list = null;
            SQLiteDatabase db = this.getReadableDatabase();
            String setsreps = "";
            String selectQuery = "SELECT * FROM " + TABLE_WORKOUT + " WHERE " + KEY_WORKOUT_NAME + " = '" + workoutname + "';";
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            try {
                if (cursor.moveToFirst()) {
                    do {
                        setsreps = cursor.getString(cursor.getColumnIndex(KEY_SETS_REPS));
                    } while (cursor.moveToNext());
                }
            }
            catch(SQLiteException e)
            {
                throw new Error("Unable to get data from database");
            }
            try {
                cursor.close();
            }
            catch (SQLiteException e)
            {
                throw new Error("Unable to close cursor");
            }

            //returning list spliting and removing commas
            list = new ArrayList(Arrays.asList(setsreps.split("\\s*,\\s*")));
            return list;
        }
    public Workout[] getAllWorkouts()
    {

        ArrayList<Workout> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_WORKOUT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
            try {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {

                    do
                    {
                        Workout workout = new Workout();
                        workout.workoutname = cursor.getString(cursor.getColumnIndex(KEY_WORKOUT_NAME));
                        workout.date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                        workout.setsreps = cursor.getString(cursor.getColumnIndex(KEY_SETS_REPS));
                        workout.exercisenames = cursor.getString(cursor.getColumnIndex(KEY_EXERCISES));
                        workout.duration = cursor.getInt((cursor.getColumnIndex(KEY_DURATION)));
                        workout.smusclegroups = cursor.getString(cursor.getColumnIndex(KEY_MUSCLE_GROUP));
                        workout.saved = cursor.getInt(cursor.getColumnIndex(KEY_SAVED));

                        list.add(workout);

                    } while (cursor.moveToNext());

                }

            }
            finally
            {
                try
                {
                    cursor.close();
                }
                catch (SQLiteException e)
                {
                    throw new Error("Unable to close cursor");
                }
            }
        //converting list to array and returning it
        Workout[] workoutlist = list.toArray(new Workout[list.size()]);
        return workoutlist;
        }

        //getting a list of objects of workout
        public Workout[] getAllSaved(int saved)
        {

            ArrayList<Workout> list = new ArrayList<>();

            // Select All Query
            String selectQuery = "SELECT * FROM " + TABLE_WORKOUT + " WHERE " + KEY_SAVED + " = '" + saved + "';";
            SQLiteDatabase db = this.getReadableDatabase();


                Cursor cursor = db.rawQuery(selectQuery, null);
                try {

                    // looping through all rows and adding to list
                    if (cursor.moveToFirst()) {

                        do {
                            Workout workout = new Workout();
                            workout.workoutname = cursor.getString(cursor.getColumnIndex(KEY_WORKOUT_NAME));
                            workout.date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
                            workout.setsreps = cursor.getString(cursor.getColumnIndex(KEY_SETS_REPS));
                            workout.exercisenames = cursor.getString(cursor.getColumnIndex(KEY_EXERCISES));
                            workout.duration = cursor.getInt((cursor.getColumnIndex(KEY_DURATION)));
                            workout.smusclegroups = cursor.getString(cursor.getColumnIndex(KEY_MUSCLE_GROUP));
                            workout.saved = cursor.getInt(cursor.getColumnIndex(KEY_SAVED));

                            list.add(workout);

                        } while (cursor.moveToNext());

                    }

                }
                finally
                {
                    try
                    {
                        cursor.close();
                    }
                    catch (SQLiteException e)
                    {
                        throw new Error("Unable to create database");
                    }
                }


            //converting list to array and returning it
            Workout[] workoutlist = list.toArray(new Workout[list.size()]);
            return workoutlist;
        }

        //getting workout
        public Workout getWorkout(String workoutname)
        {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery =  "SELECT * " + " FROM " + TABLE_WORKOUT + " WHERE " + KEY_WORKOUT_NAME + " = '" + workoutname + "';" ;


            //creating new object workout and adding values to it
            Workout workout = new Workout();

            Cursor cursor = db.rawQuery(selectQuery,null);
            try
            {
                if (cursor.moveToFirst()) {
                    do {

                        workout.workoutname = cursor.getString(cursor.getColumnIndex(KEY_WORKOUT_NAME));
                        workout.date =cursor.getString(cursor.getColumnIndex(KEY_DATE));
                        workout.setsreps  =cursor.getString(cursor.getColumnIndex(KEY_SETS_REPS));
                        workout.exercisenames = cursor.getString(cursor.getColumnIndex(KEY_EXERCISES));
                        workout.duration = cursor.getInt(cursor.getColumnIndex(KEY_DURATION));
                        workout.smusclegroups = cursor.getString(cursor.getColumnIndex(KEY_MUSCLE_GROUP));
                        workout.saved = cursor.getInt(cursor.getColumnIndex(KEY_SAVED));

                    } while (cursor.moveToNext());
                }

            }catch (SQLiteException e)
            {
                throw new Error("Unable to get data from database");
            }

            finally
            {
                try
                {
                    cursor.close();
                }
                catch (SQLiteException e)
                {
                    throw new Error("Unable to close cursor");
                }
            }
            return workout;
        }
        //deleting workout from database
        public void delete(String workoutname)
        {
            SQLiteDatabase db = this.getWritableDatabase();
            try
            {
                db.delete(TABLE_WORKOUT, KEY_WORKOUT_NAME + "= ?", new String[]{String.valueOf(workoutname)});

            }
            catch (SQLiteException e)
            {
                throw new Error("Unable to delete workout");
            }

        }

}
