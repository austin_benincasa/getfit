package com.example.elona.getfit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LogIn extends AppCompatActivity implements View.OnClickListener
{
    //initialize GUI elements loginButton, etUsername & etPassword (EditTexts), and createAccountLink (TextView)
    Button LoginButton;
    EditText etUsername, etPassword;
    TextView createAccountLink;

    //create user storage database
    UserStorage usdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        //find GUI elements by ID from the XML layout file
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        LoginButton = (Button) findViewById(R.id.LoginButton);
        createAccountLink = (TextView) findViewById(R.id.createAccountLink);

        //set listener for login button and create account link
        LoginButton.setOnClickListener(this);
        createAccountLink.setOnClickListener(this);

        //if a user is already registered on device,
        //set create account link to invisible
        //(does not allow multiple users per device)
        usdb = new UserStorage(getApplicationContext());
        if (usdb.checkdatabase() == 1) {
            createAccountLink.setVisibility(View.GONE);
        }
    }

    //listener method
    @Override
    public void onClick(View v)
    {
        //get the id of the button selected
        switch(v.getId())
        {
            //if the button selected is the log in button,
            case R.id.LoginButton:

                //save username and password as Strings
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                //make sure username and password match
                //if they match, take user to the main screen activity
                if (usdb.CheckLogin(username, password) == true)
                {
                    Intent i = new Intent(this,MainActivity.class);
                    //i.putExtra("username", username);
                    startActivity(i);

                }
                //if they do not match, refresh the login page and allow the user to try again
                else
                {
                    //print error message to user
                    Context context = getApplicationContext();
                    CharSequence errorMsg = "Wrong username or password. Try again.";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, errorMsg, duration);
                    toast.show();

                    startActivity(new Intent(this, LogIn.class));
                }

                //add username to shared preferences to be accessed in other activities
                SharedPreferences.Editor editor = getSharedPreferences("USER_PREFS", MODE_PRIVATE).edit();
                editor.putString("username", username);
                editor.commit();

                break;

            //if the user clicks on the create account link,
            //close the database
            //start a new create account activity
            case R.id.createAccountLink:
                usdb.close();
                startActivity(new Intent(this, createAccount.class));
                break;
        }
    }
}