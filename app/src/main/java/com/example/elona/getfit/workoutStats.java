package com.example.elona.getfit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Calendar;
import java.util.*;


public class workoutStats extends BaseMenuClass implements View.OnClickListener
{
    //set string array exercisenames and difficulty int
    String[] musclegroups;
    int difficulty;
    String workoutname;
    int prevActivity;
    ArrayList<Exercise> exerciseList;
    int time;
    int metscore;
    int weight;
    UserStorage us;
    User user;
    double calories;
    double temp;
    String timestring;
    Workout workout;
    Workout savedWorkout;

    //set GUI elements button, textview, and progressbar
    Button bSaveWorkout, bDone;
    TextView tvMusclesTargeted, tvOr, tvTime, tvCalBurned;
    ProgressBar finishedProgressBar;
    WorkoutStorage work;

    //data variables
    Calendar c = Calendar.getInstance();
    String date = "";

    //lists for saving workouts
    ArrayList<String> listexercise = new ArrayList<>();
    ArrayList<String> listsetsreps = new ArrayList<>();
    ArrayList<String> listmuscles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_stats);

        us = new UserStorage(this);

        //getting list of exercises from activity
        Intent intent = getIntent();
        difficulty = intent.getIntExtra("int_difficulty", 1);
        workoutname = intent.getStringExtra("string_workoutname");
        musclegroups = intent.getStringArrayExtra("musclegroups");
        prevActivity = intent.getIntExtra("prevActivity", 1);
        exerciseList = (ArrayList<Exercise>)getIntent().getSerializableExtra("exerciselist");
        time = intent.getIntExtra("time", 0);
        metscore = intent.getIntExtra("metscore", 0);
        timestring = intent.getStringExtra("stringtime");

        //find id of GUI elements in XML layout file
        bSaveWorkout = (Button) findViewById(R.id.bSaveWorkout);
        bDone = (Button) findViewById(R.id.bDone);
        tvMusclesTargeted = (TextView) findViewById(R.id.tvMusclesTargeted);
        tvTime = (TextView) findViewById(R.id.tvTime);
        tvCalBurned = (TextView) findViewById(R.id.tvCalBurned);

        //calculate calories
        SharedPreferences pref = getSharedPreferences("USER_PREFS",MODE_PRIVATE);
        String username = pref.getString("username",null);
        user = us.getUser(username);
        weight = user.weight;
        temp = (metscore * (weight / 2.2));
        calories = (temp * (time));
        tvCalBurned.setText(String.valueOf(Math.round(calories)));
        us.close();

        //setting time
        tvTime.setText(timestring);

        tvOr = (TextView) findViewById(R.id.tvOr);
        finishedProgressBar = (ProgressBar) findViewById(R.id.finishedProgressBar);

        //since the user will be finished with the workout when he/she reaches this activity,
        //set the progressbar to its max value (finished)
        finishedProgressBar.setMax(1);
        finishedProgressBar.setProgress(1);
        finishedProgressBar.setScaleY(3f);

        bSaveWorkout.setOnClickListener(this);
        bDone.setOnClickListener(this);
        String musclegroup = "";

        //putting muscle groups in string
        for(int e = 0; e < musclegroups.length; e++)
        {
            musclegroup += musclegroups[e];
            if(e + 1 < musclegroups.length)
            {
                musclegroup += ",";
            }
        }
        tvMusclesTargeted.setText(musclegroup);

        //if comming from user history or saved workout user doesnt need to resave
        if (prevActivity == 1)
        {
            bSaveWorkout.setVisibility(View.GONE);
            tvOr.setVisibility(View.GONE);
        }

        work = new WorkoutStorage(this);

        //setting date
        date += c.get(Calendar.MONTH);
        date += "-";
        date += c.get(Calendar.DAY_OF_MONTH);
        date += "-";
        date += c.get(Calendar.YEAR);

        //getting infor ready for saving
        for(int i = 0; i < exerciseList.size(); i++)
        {
            listexercise.add(exerciseList.get(i).exercisename);
            listsetsreps.add(String.valueOf(exerciseList.get(i).sets));
            listsetsreps.add(String.valueOf(exerciseList.get(i).reps));
        }
        for(int c = 0; c < musclegroups.length; c++)
        {
            listmuscles.add(musclegroups[c]);
        }
        workout = new Workout(workoutname,listexercise,listsetsreps,listmuscles,date,listexercise.size(),0);
        savedWorkout = new Workout(workoutname,listexercise,listsetsreps,listmuscles,date,listexercise.size(),1);
    }

    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent(this, MainActivity.class);

        int id = v.getId();

        //if user wants to save workout
        if (id == R.id.bSaveWorkout)
        {
            //print message to user

                Context context = getApplicationContext();
                CharSequence errorMsg = "Workout saved!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, errorMsg, duration);
                toast.show();

            //save the workout
            work.addWorkout(savedWorkout);

        }
        //if user does not want to save workout
        else if (id == R.id.bDone)
        {
            //save workout for history
            work.addWorkout(workout);
        }

        work.close();
        startActivity(intent);
        this.finish();

    }
}
