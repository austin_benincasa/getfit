package com.example.elona.getfit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.io.IOException;
import java.sql.SQLException;

//The GenerateWorkout activity tak0es in user input (duration, difficulty level, muscle group user wishes to work out) as parameters to create a workout object.
public class generateWorkout extends BaseMenuClass implements View.OnClickListener
{
    String [] exercisenames;
    Exercise [] exercisearray;
    String [] musclegroups = new String[]{""};

    //initialize spinners, button to generate workout, and array to populate duration spinner
    Spinner difficulty_spinner, muscle_group_spinner, duration_spinner;
    Button bGenerate;
    Integer[] duration_array = new Integer[]{2,3,4};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //set layout using xml file
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_workout);

        //find spinners by id from .xml layout file
        difficulty_spinner = (Spinner) findViewById(R.id.difficulty_spinner);
        muscle_group_spinner = (Spinner) findViewById(R.id.muscle_group_spinner);
        duration_spinner = (Spinner) findViewById(R.id.duration_spinner);
        bGenerate = (Button) findViewById(R.id.bGenerate);

        //create array adapters to listen for user input
        ArrayAdapter<CharSequence> difficulty_adapter = ArrayAdapter.createFromResource(this, R.array.difficulty_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> muscle_group_adapter = ArrayAdapter.createFromResource(this, R.array.muscle_group_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<Integer> duration_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, duration_array);

        //set items for spinner adapters
        difficulty_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        muscle_group_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        duration_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //set adapters to their respective spinners
        difficulty_spinner.setAdapter(difficulty_adapter);
        muscle_group_spinner.setAdapter(muscle_group_adapter);
        duration_spinner.setAdapter(duration_adapter);

        //set listener for generate button
        bGenerate.setOnClickListener(this);

    }

    //listener method for generate button
    @Override
    public void onClick(View v)
    {
        //find id of button selected
        switch (v.getId())
        {
            //if the button selected is the generate button,
            case R.id.bGenerate:
                int diff;
                //save user input: difficulty as a String, muscle group as a String, and duration as an int
                String difficulty = (difficulty_spinner.getSelectedItem().toString());
                if(difficulty.equals("Beginner"))
                {
                    diff = 1;
                }
                else if(difficulty.equals("Intermediate"))
                {
                    diff = 2;
                }
                else
                {
                    diff = 3;
                }
                String muscle_group = muscle_group_spinner.getSelectedItem().toString();
                int duration = Integer.parseInt(duration_spinner.getSelectedItem().toString());

                //start workout overview activity, passing in duration, muscle group, and difficulty as parameters.
                //Then, start the workout overview activity.
                Intent i = new Intent(this, workoutOverview.class);
                i.putExtra("int_duration", duration);
                i.putExtra("muscle_group", muscle_group);
                i.putExtra("int_difficulty", diff);
                i.putExtra("int_activity", 0);
                startActivity(i);
                break;
        }
    }
}
