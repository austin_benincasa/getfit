package com.example.elona.getfit;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class createAccount extends AppCompatActivity implements View.OnClickListener
{

    //Initialize GUI components that take in user information
    EditText etFirstName, etLastName, etUsername, etPassword, etAge, etHeight, etWeight;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    Button createAccountButton;

    public UserStorage us;

    //onCreate method sets up variables, data and listeners for buttons and user input
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        //find GUI elements by id and set them
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        createAccountButton = (Button) findViewById(R.id.createAccountButton);
        etAge = (EditText) findViewById(R.id.etAge);
        etHeight = (EditText) findViewById(R.id.etHeight);
        etWeight = (EditText) findViewById(R.id.etWeight);

        rgGender = (RadioGroup) findViewById(R.id.rgGender);
        rbMale = (RadioButton) findViewById(R.id.rbMale);
        rbFemale = (RadioButton) findViewById(R.id.rbFemale);

        //set listeners for buttons
        createAccountButton.setOnClickListener(this);
        rbMale.setOnClickListener(this);
        rbFemale.setOnClickListener(this);

        us = new UserStorage(getApplicationContext());
    }

    //onClick method is a listener for when a view is clicked
    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            //If the create account button is clicked,
            case R.id.createAccountButton:

                //initialize gender to 0 (user is male, default radio button setting)
                int gender = 0;

                //if the user selects the female radio button, save gender as 1
                int selectedId = rgGender.getCheckedRadioButtonId();
                if (selectedId == rbFemale.getId())
                    gender = 1;

                //get user input from edit texts
                String firstName = etFirstName.getText().toString();
                String lastName = etLastName.getText().toString();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                //if the user left any of the text fields blank, display error message
                if (firstName.equals("") || lastName.equals("") || username.equals("") || password.equals("") ||
                        etAge.getText().toString().equals("") || etHeight.getText().toString().equals("") || etWeight.getText().toString().equals(""))
                {
                    //print error message to user
                    Context context = getApplicationContext();
                    CharSequence errorMsg = "ERROR: One or more fields left blank. Try again.";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, errorMsg, duration);
                    toast.show();
                }
                //if the user entered correct information into all text fields, create new user object and save
                else
                {
                    int age = Integer.parseInt(etAge.getText().toString());
                    int height = Integer.parseInt(etHeight.getText().toString());
                    int weight = Integer.parseInt(etWeight.getText().toString());

                    //create new object user, with user input as attributes
                    User registeredUser;
                    registeredUser = new User(username, password, firstName, lastName, age, height, weight, gender);

                    //add user to the database
                    us.addUser(registeredUser);

                    //start a new main activity
                    Intent i = new Intent(this, MainActivity.class);
                    i.putExtra("username", username);
                    startActivity(i);
                }

                break;

        }
    }
}
