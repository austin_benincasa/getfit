package com.example.elona.getfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by Elona on 11/5/2015.
 */

//The base menu class creates a main menu which will be visible in all activities except login and createaccount.
public class BaseMenuClass extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Listener method for menu items clicked
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        //get the id of the menu item selected
        switch (item.getItemId())
        {
            //if the menu item selected is the home item, start a new main activity
            case R.id.menu_home:
                startActivity(new Intent(this, MainActivity.class));
                this.finish();
                break;
            //if the menu item selected is the profile item, break
            //this functionality has not been added yet as of 11/11/2015
            case R.id.menu_profile:
                startActivity(new Intent(this, UserProfile.class));
                this.finish();
                break;
            //if the menu item selected is the history item, start a new history activity
            case R.id.menu_history:
                startActivity(new Intent(this, UserHistory.class));
                this.finish();
                break;
            //if the menu item selected is the about item, break
            //this functionality has not been added yet as of 11/11/2015
            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                this.finish();
                break;
        }

        //return item selected boolean
        return super.onOptionsItemSelected(item);
    }
}
