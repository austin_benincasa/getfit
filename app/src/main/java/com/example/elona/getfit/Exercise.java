package com.example.elona.getfit;

import java.io.Serializable;

/**
 * Created by Austin on 10/27/15.
 */
public class Exercise implements Serializable {
    int reps;
    int sets;
    int difficulty;
    int time;
    int met;
    String exercisename;
    String musclegroup;

    //empty constructor
    public Exercise()
    {

    }

    //exercise constructor
    public Exercise(String exercisename, String musclegroup, int difficulty, int reps, int sets,int time,int met)
    {

        this.difficulty = difficulty;
        this.reps = reps;
        this.sets = sets;
        this.exercisename = exercisename;
        this.musclegroup = musclegroup;
        this.time = time;
        this.met = met;
    }
    public Exercise(String exercisename, int sets,int reps)
    {
        this.reps = reps;
        this.sets = sets;
        this.exercisename = exercisename;

    }
    public Exercise(String exercisename, int sets)
    {
        this.sets = sets;
        this.exercisename = exercisename;

    }
    public Exercise(String exercisename)
    {
        this.exercisename = exercisename;

    }

    //setting difficulty
    public void SetDifficulty(int difficulty)
    {
        this.difficulty = difficulty;
    }
    //getting diffuculty
    public int GetDifficulty()
    {
        return this.difficulty;
    }
    //setting reps
    public void SetReps(int reps)
    {
        this.reps = reps;
    }
    //getting reps
    public int GetReps()
    {
        return this.reps;
    }
    //setting sets
    public void SetSets(int sets)
    {
        this.sets = sets;
    }
    //getting sets
    public int GetSets()
    {
        return this.sets;
    }
    //setting exercise name
    public void SetExercisename(String exercisename)
    {
        this.exercisename = exercisename;
    }
    //getting exercise name
    public String GetExercisename()
    {
        return this.exercisename;
    }
    //setting muscle group
    public void SetMusclegroup(String musclegroup)
    {
        this.musclegroup = musclegroup;
    }
    //getting muscle group
    public String GetMusclegroup()
    {
        return this.musclegroup;
    }
    //setting time
    public void SetTime(int time)
    {
        this.time = time;
    }
    //getting time
    public int GetTime()
    {
        return this.time;
    }
    //setting calories
    public void SetMET(int met)
    {
        this.met = met;
    }
    //getting calories
    public int GetMET()
    {
        return this.met;
    }
}
