package com.example.elona.getfit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;

public class UserHistory extends BaseMenuClass implements View.OnClickListener, Serializable
{
    TextView tvUserMsg, tvNoWorkoutMsg;
    LinearLayout llDispHistory;
    LinearLayout.LayoutParams buttonParams;

    Workout[] allWorkouts;
    WorkoutStorage work;
    int numWorkouts = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_history);

        tvUserMsg = (TextView) findViewById(R.id.tvUserMsg);
        //tvNoWorkoutMsg = (TextView) findViewById(R.id.tvNoWorkoutsMsg);
        llDispHistory = (LinearLayout) findViewById(R.id.llDispHistory);

        //This functionality is not working yet as of 11/12/2015
        LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(960, LinearLayout.LayoutParams.WRAP_CONTENT);

        work = new WorkoutStorage(getApplicationContext());
        allWorkouts = work.getAllWorkouts();

        //get number of completed workouts from database,
        //if no workouts, print message
        numWorkouts = work.getnumberofworkouts();
        if (numWorkouts == 0)
        {
            TextView tvNoWorkouts = new TextView(this);
            tvNoWorkouts.setText("You have not completed any workouts");
            tvNoWorkouts.setLayoutParams(buttonParams);
            llDispHistory.addView(tvNoWorkouts);
        }

        //for each completed workout,
        //set button id, and add button to view
        //get workout data from saved workouts array
        //create button with text "workout name  date completed"
        int i;
        for(i = 0; i < numWorkouts; i++)
        {
            Button bPastWorkout = new Button(this);
            bPastWorkout.setId(i + 1);
            bPastWorkout.setLayoutParams(buttonParams);
            llDispHistory.addView(bPastWorkout);
            bPastWorkout.setOnClickListener(this);

            Workout currentWorkout;
            currentWorkout = allWorkouts[i];
            String workoutName = currentWorkout.GetWorkout_Name();
            String workoutDate = currentWorkout.GetDate();
            bPastWorkout.setText(workoutName + " " + workoutDate);
        }
    }

    @Override
    public void onClick(View v)
    {
        //get id of the button that was selected
        int buttonID = v.getId();
        Workout workout = work.getWorkout(allWorkouts[buttonID-1].workoutname);
        Intent i = new Intent(this, workoutOverview.class);
        i.putExtra("workout",workout);
        i.putExtra("prevActivity", 1);
        startActivity(i);
    }
}