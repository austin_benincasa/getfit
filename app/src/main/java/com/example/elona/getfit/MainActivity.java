package com.example.elona.getfit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//The main activity class is the 'main' or 'home' screen of the app. It is the first activity the user will see
//after logging into their account or after creating a new account.
public class MainActivity extends BaseMenuClass implements View.OnClickListener
{
    //initialize buttons
    Button bLogout, bGenerateWorkout, bCreateWorkout, bSavedWorkouts;
    TextView tvUserMsg;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get username from shared preferences
        SharedPreferences prefs = getSharedPreferences("USER_PREFS", MODE_PRIVATE);
        String username = prefs.getString("username", null);

        //find buttons by ID from the XML layout file
        bLogout = (Button) findViewById(R.id.bLogout);
        bGenerateWorkout = (Button) findViewById(R.id.bGenerateWorkout);
        bCreateWorkout = (Button) findViewById(R.id.bCreateWorkout);
        bSavedWorkouts = (Button) findViewById(R.id.bSavedWorkouts);
        tvUserMsg = (TextView) findViewById(R.id.tvUserMsg);

        //print welcome message to user
        tvUserMsg.setText("Hi, " + username + "!");

        //set listeners for log out and generate workout buttons
        bLogout.setOnClickListener(this);
        bGenerateWorkout.setOnClickListener(this);
        bCreateWorkout.setOnClickListener(this);
        bSavedWorkouts.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        //get the id of the button clicked
        switch(v.getId())
        {
            //if the button clicked is the logout button,
            //take the user to the log in screen/activity
            case R.id.bLogout:

                //if user logs out, close shared preferences
                SharedPreferences prefs = getSharedPreferences("USER_PREFS",MODE_PRIVATE);
                prefs.edit().clear().commit();

                startActivity(new Intent(this, LogIn.class));
                break;

            //if the button clicked is the generate workout button,
            //take the user to the generate workout screen/activity
            case R.id.bGenerateWorkout:
                startActivity(new Intent(this, generateWorkout.class));
                break;

            //if the button clicked is the create workout button, break;
            //this functionality has not been added yet as of 11/11/2015
            case R.id.bCreateWorkout:
                startActivity(new Intent(this, userCustomWorkout.class));
                break;

            //if the button clicked is the saved workouts button,
            //take the user to the saved workouts screen/activity
            case R.id.bSavedWorkouts:
                startActivity(new Intent(this, savedWorkouts.class));
                break;
        }
    }
}

