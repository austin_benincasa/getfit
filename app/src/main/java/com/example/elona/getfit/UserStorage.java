package com.example.elona.getfit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class UserStorage extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Users Table";

    // User table name
    private static final String TABLE_USERS = "Users";

    // User Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_FIRST_NAME = "first_name";
    private static final String KEY_LAST_NAME = "last_name";
    private static final String KEY_AGE = "age";
    private static final String KEY_HEIGHT = "height";
    private static final String KEY_WEIGHT = "weight";
    private static final String KEY_GENDER = "gender";

    public UserStorage(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //create table query
    String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USERS + "(" +
            KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_USERNAME + " TEXT," + KEY_PASSWORD + " TEXT,"
            + KEY_FIRST_NAME + " TEXT," + KEY_LAST_NAME + " TEXT," + KEY_AGE + " INTEGER, " + KEY_HEIGHT + " INTEGER, " + KEY_WEIGHT + " INTEGER, "
            + KEY_GENDER + " INTEGER" + ");";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_USER_TABLE); // drop table if exists
        onCreate(db);
    }

    //check if user has created an account already
    public int checkdatabase()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_USERS + " WHERE " + KEY_ID + " = '" + 1 + "';";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0)
        {
            return 1;
        }
        return 0;
    }
    //adding user to databse
    public long addUser(User user)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USERNAME, user.GetUsername());
        values.put(KEY_PASSWORD, user.GetPassword());
        values.put(KEY_FIRST_NAME, user.GetFirstname());
        values.put(KEY_LAST_NAME, user.GetLastname());
        values.put(KEY_AGE, user.GetAge());
        values.put(KEY_HEIGHT, user.GetHeight());
        values.put(KEY_WEIGHT, user.GetWeight());
        values.put(KEY_GENDER, user.GetGender());
        long insert = db.insert(TABLE_USERS, null, values);
        return insert;
    }

    //function checks if user is logged in
    public boolean CheckLogin(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();

        //query for database
        String selectQuery = "SELECT * FROM " + TABLE_USERS + " WHERE " + KEY_USERNAME + " = '" + username +  "' AND " +
                KEY_PASSWORD + " = '" + password + "';";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() < 1)
        {
            return false;
        }
        return true;
    }

    //function deletes entry in database
    public void deleteEntry(String username)
    {
        //delete
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_USERS + " WHERE " + KEY_USERNAME + " ='" + username + "';");

    }

    //function gets user information from database
    public User getUser(String username) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_USERS + " WHERE "
                + KEY_USERNAME + " = '" + username + "';";

        Cursor cursor = db.rawQuery(selectQuery, null);

        //creating object user and adding values
        User user = new User();
        try {
            if (cursor.moveToFirst()) {
                do {
                    user.username = cursor.getString(cursor.getColumnIndex(KEY_USERNAME));
                    user.password = cursor.getString(cursor.getColumnIndex(KEY_PASSWORD));
                    user.firstname = cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME));
                    user.lastname = cursor.getString(cursor.getColumnIndex(KEY_LAST_NAME));
                    user.age = cursor.getInt(cursor.getColumnIndex(KEY_AGE));
                    user.height = cursor.getInt(cursor.getColumnIndex(KEY_HEIGHT));
                    user.weight = cursor.getInt(cursor.getColumnIndex(KEY_WEIGHT));
                    user.gender = cursor.getInt(cursor.getColumnIndex(KEY_GENDER));

                } while (cursor.moveToNext());
            }
        }
        catch(SQLiteException e)
        {

            throw new Error("Unable to get data from database");

        }
        try {
            cursor.close();
        }
        catch(SQLiteException e)
        {

            throw new Error("Unable to close cursor");

        }
        return user;
    }

    //updating a user in database
    public int updateEntry(User user)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(KEY_USERNAME, user.username);
        values.put(KEY_PASSWORD, user.password);
        values.put(KEY_FIRST_NAME, user.firstname);
        values.put(KEY_LAST_NAME, user.lastname);
        values.put(KEY_AGE, user.age);
        values.put(KEY_HEIGHT, user.height);
        values.put(KEY_WEIGHT, user.weight);
        values.put(KEY_GENDER, user.gender);
        try {
            return db.update(TABLE_USERS, values, KEY_ID + " = ?",
                    new String[]{String.valueOf(user.username)});
        }
        catch(SQLiteException e)
        {

            throw new Error("Unable to updata database");

        }
    }
}