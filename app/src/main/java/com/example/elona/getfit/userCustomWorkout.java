package com.example.elona.getfit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class userCustomWorkout extends BaseMenuClass implements View.OnClickListener, AdapterView.OnItemSelectedListener
{
    //initialize GUI elements
    Spinner muscle_group_spinner, exercise_spinner, number_reps_spinner, number_sets_spinner;
    Button bContinue, bDone;
    TextView tv_number_sets, tv_number_reps;

    //initialize arrays that hold workout info
    Integer[] reps_array = new Integer[]{3,4,5,8,12,20};
    Integer[] sets_array = new Integer[]{2,3,6,8};
    String[] exercise_array = new String[]{""};
    ArrayList <String> exnames = new ArrayList<>();
    ArrayList <String> setsreps = new ArrayList<>();
    ArrayList <String> musclegroups = new ArrayList<>();
    int duration;

    //initialize exercisestorage
    ExerciseStorage ex;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //get duration from previous activity
        Intent intent = getIntent();
        duration = intent.getIntExtra("duration", 0);

        //if another exercise was previously added,
        //add info to arrays
        if (duration > 0)
        {
            exnames = intent.getStringArrayListExtra("exnames");
            setsreps = intent.getStringArrayListExtra("setsreps");
            musclegroups = intent.getStringArrayListExtra("musclegroups");
        }

        //set layout
        setContentView(R.layout.activity_user_custom_workout);

        ex = new ExerciseStorage(this);
        try {

            ex.createDataBase();

        } catch (IOException ioe)
        {

            throw new Error("Unable to create database");

        }
        try {

            ex.openDataBase();

        }catch(SQLException sqle)
        {
            throw new Error("Unable to open database");
        }

        //find spinners by id from .xml layout file
        muscle_group_spinner = (Spinner) findViewById(R.id.muscle_group_spinner);
        exercise_spinner = (Spinner) findViewById(R.id.exercise_spinner);
        number_reps_spinner = (Spinner) findViewById(R.id.number_reps_spinner);
        number_sets_spinner = (Spinner) findViewById(R.id.number_sets_spinner);

        //find buttons by id from .xml layout file
        bContinue = (Button) findViewById(R.id.bContinue);
        bDone = (Button) findViewById(R.id.bDone);

        //find textviews by id from .xml layout file
        tv_number_sets = (TextView) findViewById(R.id.tv_number_sets);
        tv_number_reps = (TextView) findViewById(R.id.tv_number_reps);

        //set adapter to muscle group spinner
        ArrayAdapter<CharSequence> muscle_group_adapter = ArrayAdapter.createFromResource(this, R.array.muscle_group_array, android.R.layout.simple_spinner_item);
        muscle_group_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        muscle_group_spinner.setAdapter(muscle_group_adapter);
        muscle_group_spinner.setOnItemSelectedListener(this);

        //set listener for generate button
        bContinue.setOnClickListener(this);
        bDone.setOnClickListener(this);

        //do not allow user to enter more than 5 exercises
        //if user is at 5, get rid of continue button
        if (duration >= 4)
        {
            bContinue.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch (parent.getId())
        {
            //if the item selected is from the muscle group spinner
            case R.id.muscle_group_spinner:

                //get muscle group from spinner, and set exercise array to contain only exercises
                //that pertain to selected muscle group
                String muscle_group = muscle_group_spinner.getSelectedItem().toString();
                exercise_array = ex.getAllExerciseNames(muscle_group);

                //get exercise from spinner, and set adapter
                ArrayAdapter<String>  exercise_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, exercise_array);
                exercise_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                exercise_spinner.setAdapter(exercise_adapter);
                exercise_spinner.setOnItemSelectedListener(this);
                break;

            //if item selected is from exercise spinner
            case R.id.exercise_spinner:
                //get exercise selected from spinner
                String exercise = exercise_spinner.getSelectedItem().toString();

                //if the user selects jogging,
                if (exercise.equals("Jogging"))
                {
                    //set text of number sets to duration
                    //set setsarray to hold values 10,15, 30 (minutes of jogging)
                    //set numberreps textview/spinner to invisible
                    tv_number_sets.setText("Duration:");
                    sets_array = new Integer[]{10, 15, 30};
                    tv_number_reps.setVisibility(View.GONE);
                    number_reps_spinner.setVisibility(View.GONE);
                }

                //set sets adapter
                ArrayAdapter<Integer> number_sets_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sets_array);
                number_sets_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                number_sets_spinner.setAdapter(number_sets_adapter);
                number_sets_spinner.setOnItemSelectedListener(this);

                break;

            //if item selected is from sets spinner
            case R.id.number_sets_spinner:

                //get number of sets from spinner
                int numSets = Integer.parseInt(number_sets_spinner.getSelectedItem().toString());

                //set corresponding number of reps given number of sets
                if (numSets == 10 || numSets == 15 || numSets == 30) {
                }
                else
                {
                    switch(numSets){
                        case 2:
                            reps_array = new Integer[]{20};
                            break;
                        case 3:
                            reps_array = new Integer[]{5,8,12};
                            break;
                        case 6:
                            reps_array = new Integer[]{4,5};
                            break;
                        case 8:
                            reps_array = new Integer[]{3};
                            break;
                    }
                }

                //set adapter for sets adapter
                ArrayAdapter<Integer> number_reps_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, reps_array);
                number_reps_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                number_reps_spinner.setAdapter(number_reps_adapter);
                number_reps_spinner.setOnItemSelectedListener(this);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    //listener method for generate button
    @Override
    public void onClick(View v)
    {
        String muscle_group;
        String exercise;
        String sets;
        String reps;

        //find id of button selected
        switch (v.getId())
        {
            //if the button selected is the generate button,
            case R.id.bContinue:

                //getting values from spinner
                muscle_group = muscle_group_spinner.getSelectedItem().toString();
                exercise = exercise_spinner.getSelectedItem().toString();
                sets = number_sets_spinner.getSelectedItem().toString();
                reps = number_reps_spinner.getSelectedItem().toString();

                //checking jogging case
                if(exercise.equals("Jogging"))
                {
                    //adding information
                    if (musclegroups.contains(muscle_group))
                    {
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(Integer.toString(0));
                    }
                    else
                    {
                        musclegroups.add(muscle_group);
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(Integer.toString(0));
                    }
                }
                //not a jogging case
                else
                {
                    //adding information
                    if (musclegroups.contains(muscle_group))
                    {
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(reps);
                    }
                    else
                    {
                        musclegroups.add(muscle_group);
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(reps);
                    }
                }


                duration++;

                //setting intents from next class
                Intent i = new Intent(this, userCustomWorkout.class);
                i.putStringArrayListExtra("exnames", exnames);
                i.putStringArrayListExtra("setsreps", setsreps);
                i.putExtra("musclegroups", musclegroups);
                i.putExtra("duration",duration);
                startActivity(i);

                break;

            case R.id.bDone:

                //getting spinner values
                muscle_group = muscle_group_spinner.getSelectedItem().toString();
                exercise = exercise_spinner.getSelectedItem().toString();
                sets = number_sets_spinner.getSelectedItem().toString();
                reps = number_reps_spinner.getSelectedItem().toString();


                //checking jogging case
                if(exercise.equals("Jogging"))
                {
                    //adding information
                    if (musclegroups.contains(muscle_group))
                    {
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(Integer.toString(0));
                    }
                    else
                    {
                        musclegroups.add(muscle_group);
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(Integer.toString(0));
                    }

                }
                //not a jogging case
                else
                {
                    //adding infromation
                    if (musclegroups.contains(muscle_group))
                    {
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(reps);
                    }
                    else
                    {
                        musclegroups.add(muscle_group);
                        exnames.add(exercise);
                        setsreps.add(sets);
                        setsreps.add(reps);
                    }

                }
                duration++;


                //setting information to be given to next activity
                String[] exnamesarray = new String[exnames.size()];
                exnamesarray = exnames.toArray(exnamesarray);

                String[] setsrepssarray = new String[setsreps.size()];
                setsrepssarray = setsreps.toArray(setsrepssarray);

                String[] musclegroupsarray = new String[musclegroups.size()];
                musclegroupsarray = musclegroups.toArray(musclegroupsarray);

                //put information to bundle to pass to next activity
                Bundle b = new Bundle();
                b.putStringArray("exnames",exnamesarray);
                b.putStringArray("setsreps", setsrepssarray);
                b.putStringArray("musclegroups", musclegroupsarray);

                //add bundle to intent, send intent to next activity
                Intent j = new Intent(this, workoutOverview.class);
                j.putExtras(b);
                j.putExtra("prevActivity", 2);
                j.putExtra("duration",duration);
                startActivity(j);
                break;
        }
    }
}
