package com.example.elona.getfit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class workoutOverview extends BaseMenuClass implements View.OnClickListener
{
    //set ints duration and difficulty, and String muscle group
    int duration;
    int difficulty;
    int i = 0;
    String muscle_group;
    String workoutName;
    int prevActivity;
    int metscore;

    //set String array exercisenames and exercisearray array of Exercise objects
    String [] exercisenames;
    Exercise [] exercisearray;
    String [] musclegroups = new String[]{""};
    String [] setreps;

    //set button to start workout
    Button bStartWorkout;
    EditText etWorkoutName;

    //array list

    //set linear layout to programmatically display GUI elements in .java file
    LinearLayout llDispWorkout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_overview);

        //find GUI elements by id from XML layout file
        etWorkoutName = (EditText) findViewById(R.id.etWorkoutName);
        bStartWorkout = (Button) findViewById(R.id.bStartWorkout);
        bStartWorkout.setOnClickListener(this);

        llDispWorkout = (LinearLayout) findViewById(R.id.llDispWorkout);

        //get previous activity from intent
        Intent intent = getIntent();
        prevActivity = intent.getIntExtra("prevActivity", 0);

        //if previous activity was GenerateWorkout
        if (prevActivity == 0)
        {
            duration = intent.getIntExtra("int_duration", 2);
            difficulty = intent.getIntExtra("int_difficulty", 2);
            muscle_group = intent.getStringExtra("muscle_group");
            musclegroups[0] = muscle_group;

            //opening database
            ExerciseStorage ex;
            ex = new ExerciseStorage(this);
            try {

                ex.createDataBase();

            } catch (IOException ioe)
            {

                throw new Error("Unable to create database");

            }
            try {

                ex.openDataBase();

            }catch(SQLException sqle)
            {
                throw new Error("Unable to open database");
            }
            //array of exercises names
            exercisenames = new String[duration];

            //randomly getting exercises names for workout
            exercisearray = new Exercise [duration];

            //coming from generate workout
            for(int j = 0; j < duration; j++)
            {
                exercisenames[j] = ex.getExerciseByRandom(muscle_group, difficulty);
                exercisearray[j] = ex.getExerciseByName(exercisenames[j], difficulty);
            }
            ex.close();
        }

        //if previous activity was saved workouts
        else if (prevActivity == 1)
        {
            //get workout from previous activity
            Workout workout = (Workout) intent.getSerializableExtra("workout");
            workoutName = workout.workoutname;
            etWorkoutName.setText(workoutName);
            //don't allow user to change the name of the workout
            etWorkoutName.setEnabled(false);

            //get workout information
            duration = workout.GetDuration();
            String exnames = workout.GetExercise();
            exercisenames = exnames.split(",");
            String exercisesetreps = workout.GetSetsReps();
            setreps = exercisesetreps.split(",");
            Log.w("Names:",exnames);
            Log.w("SetsReps",exercisesetreps);

            //add exercises, along with reps and sets to exercise array
            exercisearray = new Exercise[duration];
            for(int j = 0; j < duration; j++)
            {
                Exercise ex = new Exercise(exercisenames[j],Integer.parseInt(setreps[i]),Integer.parseInt(setreps[(i+1)]));
                exercisearray[j] = ex;
                i = (i + 2);
            }

        }
        //if previous activity was custom workouts
        else if (prevActivity == 2)
        {
            //get workout information from intent/bundle
            duration = intent.getIntExtra("duration",0);
            Bundle b = intent.getExtras();
            exercisearray = new Exercise[duration];
            exercisenames = new String[duration];
            musclegroups = new String[duration];
            setreps = new String[duration];

            exercisenames = b.getStringArray("exnames");
            musclegroups = b.getStringArray("musclegroups");
            setreps = b.getStringArray("setsreps");
            exercisearray = new Exercise[duration];
            System.out.println(duration);

            //add exercises, along with reps and sets, to exercise array
            for(int j = 0; j < duration; j++)
            {

                Exercise ex = new Exercise(exercisenames[j], Integer.parseInt(setreps[i]), Integer.parseInt(setreps[(i + 1)]));
                exercisearray[j] = ex;

                i = (i + 2);
            }

        }

        //find button to start workout and set on click listener
        bStartWorkout = (Button) findViewById(R.id.bStartWorkout);
        bStartWorkout.setOnClickListener(this);

        //loop through each exercise in the workout
        //for each exercise, create new textview for name, reps, and sets, respectively
        //add to linear layout llDispWorkout in the GUI
        int i;
        int j;
        for(i = 0; i < duration; i++)
        {
            TextView tvExerciseNum = new TextView(this);
            j = i+1;
            tvExerciseNum.setText("Exercise " + j + ":");
            llDispWorkout.addView(tvExerciseNum);

            TextView tvExerciseName = new TextView(this);
            tvExerciseName.setText(exercisearray[i].GetExercisename());
            llDispWorkout.addView(tvExerciseName);
            metscore += exercisearray[i].GetMET();

            if (exercisearray[i].GetExercisename().equals("Jogging"))
            {
                TextView tvExerciseReps = new TextView(this);
                if (prevActivity == 2)
                {
                    tvExerciseReps.setText("" + exercisearray[i].GetSets() + " mins");
                    exercisearray[i].time = exercisearray[i].GetSets();
                }
                else {
                    tvExerciseReps.setText("" + exercisearray[i].time + " mins");

                }
                llDispWorkout.addView(tvExerciseReps);

                //create empty textview to make margin between exercises
                TextView empty = new TextView(this);
                empty.setText("");
                llDispWorkout.addView(empty);
            }
            else
            {
                TextView tvExerciseReps = new TextView(this);
                tvExerciseReps.setText("" + exercisearray[i].GetReps() + " reps");
                llDispWorkout.addView(tvExerciseReps);

                TextView tvExerciseSets = new TextView(this);
                tvExerciseSets.setGravity(10);
                tvExerciseSets.setText("" + exercisearray[i].GetSets() + " sets");
                llDispWorkout.addView(tvExerciseSets);

                //create empty textview to make margin between exercises
                TextView empty = new TextView(this);
                empty.setText("");
                llDispWorkout.addView(empty);
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        //get id of the button that was selected
        switch(v.getId())
        {
            //if button clicked is the start workout button,
            //pass information to the workout walkthrough class
            //and start the workout walkthrough activity
            case R.id.bStartWorkout:

                workoutName = etWorkoutName.getText().toString();

                //if the user doesn't enter a workout name
                if (workoutName.equals(""))
                {
                    //print error message to user
                    Context context = getApplicationContext();
                    CharSequence errorMsg = "ERROR: No workout name entered. Try again.";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, errorMsg, duration);
                    toast.show();

                }
                //if the user enters a workout name,
                else
                {
                    //pass names and difficulty to new activity
                    ArrayList<Exercise> exerciseList = new ArrayList<>(Arrays.asList(exercisearray));
                    Intent i = new Intent(this, workoutWalkthrough.class);
                    i.putExtra("exerciselist", exerciseList);
                    i.putExtra("int_difficulty", difficulty);
                    i.putExtra("prevActivity", prevActivity);
                    i.putExtra("string-workoutname", workoutName);
                    i.putExtra("musclegroups", musclegroups);
                    i.putExtra("metscore", metscore);
                    startActivity(i);
                    this.finish();
                    break;
                }
        }
    }
}
