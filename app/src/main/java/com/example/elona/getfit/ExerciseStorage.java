package com.example.elona.getfit;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.*;


import android.database.sqlite.SQLiteException;
/**
 * Created by Austin on 10/27/15.
 */
public class ExerciseStorage extends SQLiteOpenHelper
{

        private SQLiteDatabase db;

        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "Exercise Table";
        public final Context context;
        public static final String KEY_EXERCISE_NAME = "name";
        public static final String KEY_MUSCLE_GROUP = "mgroup";
        public static final String KEY_DIFFICULTY = "difficulty";
        public static final String KEY_SETS = "sets";
        public static final String KEY_REPS = "reps";
        public static final String KEY_TIME = "time";
        public static final String KEY_MET = "met";

        private static final String DB_PATH = "/data/data/com.example.elona.getfit/databases/";

        //Exercise table name
        private static final String TABLE_EXERCISES = "Exercise";

        //constructor
        public ExerciseStorage(Context context)
        {
            super(context,DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }

        //creating database
        public void createDataBase() throws IOException
        {

            boolean dbExist = checkDataBase();
            if(dbExist) {
            }
            else
            {
                this.getReadableDatabase();

                 try
                 {
                    copyDataBase();
                 }
                 catch (IOException e)
                 {

                     throw new Error("Unable to copy database");

            }
        }
    }
    //checking if database is in data section
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try
        {
            String myPath = DB_PATH + DATABASE_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }
        catch(SQLiteException e)
        {

            throw new Error("Database does not exist");

        }

        if(checkDB != null){

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }
    //copying database from asset folder
    private void copyDataBase() throws IOException{

        // Open your local db as the input stream
        InputStream myInput = context.getAssets().open(DATABASE_NAME);

        // Path to the just created empty db
        String outFileName =  DB_PATH + DATABASE_NAME;


        OutputStream myOutput = new FileOutputStream(outFileName);


        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);

        }
        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    //opening database
    public void openDataBase() throws SQLException
    {
        //Open the database
        String myPath = DB_PATH + DATABASE_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {

        if(db != null)
            db.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    //if database exists, update
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCISES);

        // Create tables again
        onCreate(db);
    }

    //getting exercise by name
    public Exercise getExerciseByName(String name, int difficulty) {
        SQLiteDatabase db = this.getReadableDatabase();

    String selectQuery =  "SELECT * FROM " + TABLE_EXERCISES + " WHERE " +
            KEY_EXERCISE_NAME + " = '" + name + "' AND "
            + KEY_DIFFICULTY  + " = '" + difficulty + "';";

    //creating exercise object
    Exercise exercise = new Exercise();

    Cursor cursor = db.rawQuery(selectQuery,null);
    try
    {
        if (cursor.moveToFirst()) {
            do
            {
                exercise.exercisename = cursor.getString(cursor.getColumnIndex(KEY_EXERCISE_NAME));
                exercise.musclegroup = cursor.getString(cursor.getColumnIndex(KEY_MUSCLE_GROUP));
                exercise.difficulty = cursor.getInt(cursor.getColumnIndex(KEY_DIFFICULTY));
                exercise.sets = cursor.getInt(cursor.getColumnIndex(KEY_SETS));
                exercise.reps = cursor.getInt(cursor.getColumnIndex(KEY_REPS));
                exercise.time = cursor.getInt(cursor.getColumnIndex(KEY_TIME));
                exercise.met = cursor.getInt(cursor.getColumnIndex(KEY_MET));

            } while (cursor.moveToNext());
        }
    }
    catch (SQLiteException e)
    {
        throw new Error("Unable to get data from database");
    }
    try
    {
        cursor.close();
    }
    catch (SQLiteException e)
    {
        throw new Error("Unable to close cursor");
    }
        return exercise;
    }
    public int getProfilesCount()
    {
        String countQuery = "SELECT  * FROM " + TABLE_EXERCISES + " WHERE " + KEY_DIFFICULTY + " = '1';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        try
        {
            cursor.close();
        }
        catch (SQLiteException e)
        {
            throw new Error("Unable to close cursor");
        }
        return cnt;
    }

    public String[] getAllExerciseNames(String musclegroup)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        int count = getProfilesCount();
        String selectQuery =  "SELECT * FROM " + TABLE_EXERCISES +  " WHERE " + KEY_DIFFICULTY + " = '1'" + "AND " +
                KEY_MUSCLE_GROUP + " = '" + musclegroup + "';";
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        ArrayList<String> exercisename = new ArrayList<String>();

        while(!cursor.isAfterLast())
        {
            exercisename.add(cursor.getString(cursor.getColumnIndex("name")));
            cursor.moveToNext();
        }
        try
        {
            cursor.close();
        }
        catch (SQLiteException e)
        {
            throw new Error("Unable to close cursor");
        }
        return exercisename.toArray(new String[exercisename.size()]);

    }
    //getting exercise by random
    public String getExerciseByRandom(String musclegroup, int difficulty)
    {
        String exname = "";

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery =  "SELECT * FROM " + TABLE_EXERCISES +  " WHERE " + KEY_DIFFICULTY + " = '" + difficulty +
            "' AND " + KEY_MUSCLE_GROUP + " = '" + musclegroup + "' ORDER BY random () LIMIT 1;";

        Cursor cursor = db.rawQuery(selectQuery,null);
        try
        {
            if (cursor.moveToFirst()) {
                do
                {
                    exname = cursor.getString(cursor.getColumnIndex(KEY_EXERCISE_NAME));
                } while (cursor.moveToNext());
            }
        }
        catch (SQLiteException e)
        {
            throw new Error("Unable to get data from database");
        }
        try
        {
            cursor.close();
        }
        catch (SQLiteException e)
        {
            throw new Error("Unable to close cursor");
        }
        //returning name of random exercise
        return exname;
    }

    //getting exercise by muscle group
    public Exercise getExerciseByGroup(String musclegroup, int difficulty)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery =  "SELECT * FROM " + TABLE_EXERCISES + " WHERE " +
                KEY_MUSCLE_GROUP + " = '" + musclegroup + "' AND "
                + KEY_DIFFICULTY  + " = '" + difficulty + "';";

        //creating new exercise object and adding values
        Exercise exercise = new Exercise();
        Cursor cursor = db.rawQuery(selectQuery,null);
        try
        {
            if (cursor.moveToFirst()) {
                do
                {
                    exercise.exercisename = cursor.getString(cursor.getColumnIndex(KEY_EXERCISE_NAME));
                    exercise.musclegroup = cursor.getString(cursor.getColumnIndex(KEY_MUSCLE_GROUP));
                    exercise.difficulty = cursor.getInt(cursor.getColumnIndex(KEY_DIFFICULTY));
                    exercise.sets = cursor.getInt(cursor.getColumnIndex(KEY_SETS));
                    exercise.reps = cursor.getInt(cursor.getColumnIndex(KEY_REPS));
                    exercise.time = cursor.getInt(cursor.getColumnIndex(KEY_TIME));
                    exercise.met = cursor.getInt(cursor.getColumnIndex(KEY_MET));

                } while (cursor.moveToNext());
            }
        }
        catch (SQLiteException e)
        {
            throw new Error("Unable to get data from database");
        }
        try
        {
            cursor.close();
        }
        catch (SQLiteException e)
        {
            throw new Error("Unable to close cursor");
        }
        //returning object of exercise
        return exercise;
    }
}

